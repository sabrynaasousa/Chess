package control;

import model.Square;
import model.Piece;
import model.Torre;
import model.SquaresException;

import java.awt.Color;
import java.util.ArrayList;

import model.Square.SquareEventListener;
import java.awt.Point;


public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVES = (new Color(0,255,127));

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMoves;


	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED,DEFAULT_COLOR_MOVES);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMoves) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMoves = colorMoves;
		

		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		int x = square.getPosition().x;
		int y = square.getPosition().y;
		System.out.println("(X = " + x + ", Y = " + y + ")");
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) throws SquaresException {
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square))
			{
				throw new SquaresException("Quadrado inválido");
			}
			if (!this.selectedSquare.equals(square) && possibleSquares.contains(square)){ 
				moveContentOfSelectedSquare(square);
				possibleSquares.clear();
			} 
			else {
				unselectSquare(square);
			}
		} 
		else {
			selectSquare(square);
		}
	}

	public void onOutEvent(Square square) {
		
		if (this.selectedSquare != square && !possibleSquares.contains(square) ) {	
			resetColor(square);
		} 
		else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);		
			else
				square.setColor(new Color(0,255,127));

		}
		
	}
		

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		
		for(int i = 0; i<possibleSquares.size(); i++)
			resetColor(possibleSquares.get(i));
			possibleSquares.clear();
			this.selectedSquare = EMPTY_SQUARE;
	}


	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}

	private void showPossibleMovesPeaoB(Square square) {
		ArrayList<Point> movesPeao = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i < movesPeao.size(); i++) {
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x+1, square.getPosition().y + 1);
			Square squareDireito = getSquare(square.getPosition().x+1, square.getPosition().y - 1);
			Square squareF = getSquare(square.getPosition().x+1, square.getPosition().y);

			if( ( square.getPosition().y != 0 && squares == squareDireito && squareDireito.havePiece() )   
					||	(square.getPosition().y != 7 && squares == squareEsquerdo && squareEsquerdo.havePiece() ) ) {

				if(squares.getPiece().getPiecePath().contains("White")) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}	
			}
		}
		
	}
	
	private void showPossibleMovesPeaoW(Square square) {
		ArrayList<Point> movesPeao = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i < movesPeao.size(); i++) {
			
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x-1, square.getPosition().y - 1);
			Square squareDireito = getSquare(square.getPosition().x-1, square.getPosition().y + 1);
			Square squareF = getSquare(square.getPosition().x-1, square.getPosition().y);
			if( ( square.getPosition().y != 7 && squares == squareDireito && squareDireito.havePiece() )   
					||	(square.getPosition().y != 0 && squares == squareEsquerdo && squareEsquerdo.havePiece() ) ) {

				if(squares.getPiece().getPiecePath().contains("Brown")) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}	
			}
			
			
		}
		
	}
	private void showPossibleMovesCavalo(Square square) {
		
		ArrayList<Point> movesCavalo = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i < movesCavalo.size(); i++) {			
			Square squares = getSquare(movesCavalo.get(i).x, movesCavalo.get(i).y);							
			
			if(square.getPiece().getPiecePath().contains("White")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getPiecePath().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}		
			}
			
		}
		
	}
	
	private void showPossibleMovesTorreB(Square square) {
		
		Torre torre = (Torre) square.getPiece();
		
		ArrayList<Point> movesTorre = torre.getMoves(square.getPosition().x, square.getPosition().y);			
		
		int i = 0;
		Square squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);		
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
		System.out.println("I = " + i);
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}	
		
		
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
		
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
				
	}
	private void showPossibleMovesBispoB(Square square) {

		ArrayList<Point> movesBispo = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		int i = 0;
		Square squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);	
		
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);		
				}
				break;
			}
			else {
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}	
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		
			
			if(squares.havePiece())
			{
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
	}
	private void showPossibleMovesRainhaB(Square square) {		

		ArrayList<Point> movesRainha = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);


		int i = 0;
		Square squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}
		System.out.println("I = " + i);
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	


		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);	

		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
				}
				break;
			}
			else {
				
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		

			if(squares.havePiece())
			{
				
				if(square.getPiece().getPiecePath().contains("White")) {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getPiecePath().contains("Brown")) {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				
				squares.setColor(this.colorMoves);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

	}
	
	private void showPossibleMovesReiB(Square square) {
		ArrayList<Point> movesRei = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		for (int i = 0; i <movesRei.size(); i++) {
			
			Square squares = getSquare(movesRei.get(i).x, movesRei.get(i).y);
			if(square.getPiece().getPiecePath().contains("White")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getPiecePath().contains("Brown")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getPiecePath().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMoves);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getPiecePath().contains("White")) {
						squares.setColor(this.colorMoves);
						possibleSquares.add(squares);
					}
				}		
			}	
			
		}
	}
	
	private void showMoves(Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getPieceName().equalsIgnoreCase("BispoB") || piece.getPieceName().equalsIgnoreCase("BispoW")) {
			showPossibleMovesBispoB(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}		
		if(piece.getPieceName().equalsIgnoreCase("ReiB") || piece.getPieceName().equalsIgnoreCase("ReiW")){
			showPossibleMovesReiB(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("TorreB") || piece.getPieceName().equalsIgnoreCase("TorreW")){
			showPossibleMovesTorreB(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("RainhaB") || piece.getPieceName().equalsIgnoreCase("RainhaW")){
			showPossibleMovesRainhaB(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("PeaoW")){
			showPossibleMovesPeaoW(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("PeaoB")){
			showPossibleMovesPeaoB(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("CavaloW")||piece.getPieceName().equalsIgnoreCase("CavaloB")){
			showPossibleMovesCavalo(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
	}
}
	

