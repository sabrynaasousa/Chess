package model;

public class SquaresException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SquaresException() { super(); }
	
	public SquaresException(String message) { super(message); }
	
	public SquaresException(String message, Throwable cause) { super(message, cause); }
	public SquaresException(Throwable cause) { super(cause); }	
}