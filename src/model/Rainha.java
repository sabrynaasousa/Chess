package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rainha extends Piece{

	public Rainha(String name, String piecePath) {
		super(name, piecePath);
	}

	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesRainha = new ArrayList<>();
		
		for(int i = y + 1; i < 8; i++) {
			Point point1 = new Point(x, i);
			movesRainha.add(point1);
		}
		for(int i = y - 1; i >= 0; i--) {
			Point point2 = new Point(x, i);
			movesRainha.add(point2);
		}
		
		for(int i = x + 1; i < 8; i++) {
			Point point3= new Point(i, y);
			movesRainha.add(point3);
		}
		for(int i = x - 1; i >= 0; i--) {
			Point point4 = new Point(i, y);
			movesRainha.add(point4);
		}
		for(int i = x, j = y;( i < 7 || j < 7) ; i++, j++) {					
			if(i == 7 || j == 7) {
				break ;
			}
			
			Point point5 = new Point(i + 1, j + 1);
			movesRainha.add(point5);
			
		}

		for(int i = x, j = y; i < 7 || j > 0; i++, j--) {

			if(i == 7 || j == 0) {
				break ;
			}
			
			Point point6 = new Point(i + 1, j - 1);
			movesRainha.add(point6);
			
		}

		for(int i = x, j = y; i>0 || j<7; i--, j++) {
				if(i == 0 || j == 7) {
				break;
			}

			Point point7 = new Point(i - 1, j + 1);
			movesRainha.add(point7);
		
		}
		for(int i = x, j = y; i > 0 || j > 0; i--, j--) {
			if(i == 0 || j == 0) {
				break;
			}

			Point point8 = new Point(i-1, j-1);
			movesRainha.add(point8);
			
		}
		
		return movesRainha;
	}
}