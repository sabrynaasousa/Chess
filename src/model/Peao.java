package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece{

	public Peao(String name, String piecePath) {
		super(name, piecePath);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		
		if(this.getPiecePath().contains("White")) {
			
			ArrayList<Point> movesPeao = new ArrayList<>();
			
			if(x == 6) {
				Point point1 = new Point(x - 1, y);
				movesPeao.add(point1);
				
				Point point2 = new Point(x - 2 , y);
				movesPeao.add(point2);
				
				Point point3 = new Point(x - 1, y - 1);
				movesPeao.add(point3);
				
				Point point4 = new Point(x - 1, y + 1);
				movesPeao.add(point4);
			}
			else {
				Point point5= new Point(x - 1, y - 1);
				movesPeao.add(point5);
				
				Point point6 = new Point(x - 1, y + 1);
				movesPeao.add(point6);
				
				Point point7 = new Point(x - 1, y);
				movesPeao.add(point7);
			}
			
			return movesPeao;
		}
		else {
			ArrayList<Point> movesPeao = new ArrayList<>();
			
			if(x == 1) {
				Point point8 = new Point(x + 1, y);
				movesPeao.add(point8);
				
				Point point9 = new Point(x + 2 , y);
				movesPeao.add(point9);
				
				Point point10 = new Point(x + 1, y + 1);
				movesPeao.add(point10);
				
				Point point11= new Point(x + 1, y - 1);
				movesPeao.add(point11);
			}
			else {
				Point point12 = new Point(x + 1, y + 1);
				movesPeao.add(point12);
				
				Point point13 = new Point(x + 1, y - 1);
				movesPeao.add(point13);
				
				Point point14 = new Point(x + 1, y);
				movesPeao.add(point14);
			}
			
			return movesPeao;
			
		}
		
	}
}