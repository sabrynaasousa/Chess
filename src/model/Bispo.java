package model;

import java.util.ArrayList;
import java.awt.Point;

public class Bispo extends Piece {

	public Bispo(String name, String piecePath) {
			super(name, piecePath);
	}
	
	public ArrayList<Point> getMoves(int x,int y){
	
		ArrayList<Point> movesBispo = new ArrayList<>();
		
		for(int i = x, j = y;( i < 7 || j < 7) ; i++, j++) {					

			if(i == 7 || j == 7) {
				break ;
			}
			
			Point point1 = new Point(i + 1, j + 1);
			movesBispo.add(point1);
			
		}

		for(int i = x, j = y; i < 7 || j > 0; i++, j--) {

			if(i == 7 || j == 0) {
				break ;
			}
			
			Point point2 = new Point(i + 1, j - 1);
			movesBispo.add(point2);
			
		}

		for(int i = x, j = y; i > 0 || j < 7; i--, j++) {

			if(i == 0 || j == 7) {
				break;
			}

			Point point3 = new Point(i - 1, j + 1);
			movesBispo.add(point3);
		
		}
		for(int i = x, j = y; i > 0 || j > 0; i--, j--) {

			if(i == 0 || j == 0) {
				break;
			}

			Point point4 = new Point(i - 1, j - 1);
			movesBispo.add(point4);
			
		}
	
		return movesBispo;
	}
}