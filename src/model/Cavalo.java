package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Piece{
	
	public Cavalo(String name, String piecePath) {
			super(name, piecePath);
	}
	
	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesCavalo = new ArrayList<>();	

		if( !(y == 6 || y ==7) ) {		
			if(!(x == 0)) {
				Point point1 = new Point(x - 1, y + 2);
				movesCavalo.add(point1);
			}		
			if(!(x == 7) ) {
				Point point2 = new Point(x + 1, y + 2);
				movesCavalo.add(point2);
			}			
		}
	
		if( !(y == 0 || y == 1) ) {
			if(!(x == 0)) {
				Point point3 = new Point(x - 1, y - 2);
				movesCavalo.add(point3);
			}
			if(!(x == 7) ) {
				Point point4 = new Point(x + 1, y - 2);
				movesCavalo.add(point4);
			}
		}

		if(!(x == 0 || x == 1) ) {
			if(!(y == 7))
			{
				Point point5 = new Point(x - 2, y + 1);
				movesCavalo.add(point5);
			}
			if(!(y == 0))
			{
				Point point6 = new Point(x - 2, y - 1);
				movesCavalo.add(point6);
			}
		}
		
		if( !(x == 7 || x == 6) ) {	
			if(!(y == 7))
			{
				Point point7 = new Point(x + 2, y + 1);	
				movesCavalo.add(point7);
			}
			if(!(y == 0) ) {
				Point point8 = new Point(x + 2, y - 1);
				movesCavalo.add(point8);
			}
		}	
		
		return movesCavalo;
	}
}