package model;

import java.awt.Point;
import java.util.ArrayList;

public class Torre extends Piece {

	public Torre(String name, String piecePath) {
		super(name, piecePath);
	}

	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesTorre = new ArrayList<>();
		
		for(int i = y + 1; i < 8; i++) {
			Point point1 = new Point(x, i);
					
			movesTorre.add(point1);
		}
		for(int i = y - 1; i >=	0; i--) {
			Point point2 = new Point(x, i);
			
			movesTorre.add(point2);
		}
		for(int i = x + 1; i < 8; i++) {
			Point point3 = new Point(i, y);
			
			movesTorre.add(point3);
		}
		for(int i = x - 1; i >= 0; i--) {
			Point point4 = new Point(i, y);
			
			movesTorre.add(point4);
		}
		return movesTorre;
		
	}
}