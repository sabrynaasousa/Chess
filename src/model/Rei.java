package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rei extends Piece {
	
	public Rei(String name, String piecePath) {
		super(name, piecePath);
	}
	
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> ReiMoves = new ArrayList<>();
		
		if (x == 0){
			Point ReiPoint = new Point(x + 1, y);
			ReiMoves.add(ReiPoint);
		}
		if (x == 0 && y != 7){
			Point ReiPoint1 = new Point(x + 1, y + 1);
			ReiMoves.add(ReiPoint1);
			
			Point ReiPoint2 = new Point(x, y + 1);
			ReiMoves.add(ReiPoint2);
		}
		if (x == 0 && y != 0){
			Point ReiPoint3 = new Point(x + 1, y - 1);
			ReiMoves.add(ReiPoint3);
		
			Point ReiPoint4 = new Point(x, y - 1);
			ReiMoves.add(ReiPoint4);
		}
		if (x == 7){
			Point ReiPoint5 = new Point(x - 1, y);
			ReiMoves.add(ReiPoint5);
		}
		if (x == 7 && y != 0){
			Point ReiPoint6 = new Point(x - 1, y - 1);
			ReiMoves.add(ReiPoint6);
			
			Point ReiPoint7 = new Point(x, y - 1);
			ReiMoves.add(ReiPoint7);
		}
		if (x == 7 && y != 7){
			Point ReiPoint8 = new Point(x - 1, y + 1);
			ReiMoves.add(ReiPoint8);
			
			Point ReiPoint9 = new Point(x, y + 1);
			ReiMoves.add(ReiPoint9);
		}
		if (x != 0 && x != 7){
			Point ReiPoint10 = new Point(x - 1, y);
			ReiMoves.add(ReiPoint10);
								
			Point ReiPoint11 = new Point(x + 1, y);
			ReiMoves.add(ReiPoint11);
		}
		if (x != 0 && x != 7 && y != 0){
			Point ReiPoint12 = new Point(x, y - 1);
			ReiMoves.add(ReiPoint12);
			
			Point ReiPoint13 = new Point(x - 1, y - 1);
			ReiMoves.add(ReiPoint13);
			
			Point ReiPoint14 = new Point(x + 1, y - 1);
			ReiMoves.add(ReiPoint14);
		}
		if (x != 0 && x != 7 && y != 7){
			Point ReiPoint15 = new Point(x, y + 1);
			ReiMoves.add(ReiPoint15);
	
			Point ReiPoint16 = new Point(x - 1, y + 1);
			ReiMoves.add(ReiPoint16);
			
			Point ReiPoint17 = new Point(x + 1, y + 1);
			ReiMoves.add(ReiPoint17);
		
		}
		
		System.out.println (x);
		System.out.println (y);
	
		return ReiMoves;		
	}
}