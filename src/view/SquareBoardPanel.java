package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import model.*;

import javax.swing.JPanel;

import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorMoves = new Color(0,255,127);

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorMoves);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		Piece brownPeao = new Peao("PeaoB","icon/Brown P_48x48.png");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(brownPeao);
		}

		Piece brownTorre = new Torre("TorreB","icon/Brown R_48x48.png");
		this.squareControl.getSquare(0, 0).setPiece(brownTorre);
		this.squareControl.getSquare(0, 7).setPiece(brownTorre);

		Piece brownCavalo = new Cavalo("CavaloB","icon/Brown N_48x48.png"); 
		this.squareControl.getSquare(0, 1).setPiece(brownCavalo);
		this.squareControl.getSquare(0, 6).setPiece(brownCavalo);

		Piece brownBispo = new Bispo("Bispo","icon/Brown B_48x48.png");
		this.squareControl.getSquare(0, 2).setPiece(brownBispo);
		this.squareControl.getSquare(0, 5).setPiece(brownBispo);

		Piece brownRainha = new Rainha("RainhaB","icon/Brown Q_48x48.png"); 
		this.squareControl.getSquare(0, 4).setPiece(brownRainha);

		Piece brownRei = new Rei("ReiB","icon/Brown K_48x48.png");
		this.squareControl.getSquare(0, 3).setPiece(brownRei);
		
		Piece whitePeao = new Peao("PeaoW","icon/White P_48x48.png");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(whitePeao);
		}
		
		Piece whiteTorre = new Torre("TorreW","icon/White R_48x48.png");
		this.squareControl.getSquare(7, 0).setPiece(whiteTorre);
		this.squareControl.getSquare(7, 7).setPiece(whiteTorre);

		Piece whiteCavalo = new Cavalo("CavaloW","icon/White N_48x48.png");
		this.squareControl.getSquare(7, 1).setPiece(whiteCavalo);
		this.squareControl.getSquare(7, 6).setPiece(whiteCavalo);

		Piece whiteBispo = new Bispo("BispoW","icon/White B_48x48.png");
		this.squareControl.getSquare(7, 2).setPiece(whiteBispo);
		this.squareControl.getSquare(7, 5).setPiece(whiteBispo);

		Piece whiteRainha = new Rainha("RainhaW","icon/White Q_48x48.png");
		this.squareControl.getSquare(7, 4).setPiece(whiteRainha);

		Piece whiteRei = new Rei("ReiW","icon/White K_48x48.png");
		this.squareControl.getSquare(7, 3).setPiece(whiteRei);
	}
}